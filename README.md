rarbg-dl.sh
===========

This script makes it easy to search and download torrent metainfo (magent links and .torrent files) using the 376 MB
RARBG database dump that is floating around.

See the long comment in [rarbg-dl.sh](./rarbg-dl.sh) for example usage and more info.
