#!/usr/bin/env bash

# rarbg-dl.sh
#
# In 2023, one of the most comprehensive and well-organized public torrent sites, RARBG, was abruptly taken offline,
# leaving many without a good source for content. Luckily, a 376 MB database dump (see [1], [2]) is floating around the
# internet, but it is clumsy to use, requiring the user to write SQL queries and chain together several commands.
#
# This script makes it easy to search and download torrent metainfo (magent links and .torrent files) using the
# aforementioned dump.
#
# To use, unpack the database in your downloads folder, or set RARBG_DB_PATH in your login scripts (~/.profile or
# similar) and log out and back in again. Then, invoke it like this to search for Fritz Lang's 1927 classic "Metropolis"
# (the percent sign is a wildcard):
#
# ./rarbg-dl.sh 'Metropolis%'
#
# This should locate a handful of torrents. You can narrow the search by adding things like release year, source media,
# etc:
#
# ./rarbg-dl.sh 'Metropolis%1927%BluRay%'
#
# When you've found a torrent you're interested in, use the --magnet flag to get a magnet link, or --cache=provider to
# download a .torrent file from one of the supported cache sites.
#
# Some tips:
#
# - Use a wildcard between words. For example, search for 'Star%Trek' instead of 'Star Trek'. This is helpful because
#   there is no universal word separator character - "Star Trek", "Star.Trek", and "Star_Trek" are all common.
#
# - Always use single quotes around your search term to avoid potential conflicts with your shell.
#
# - RARBG was very well-organized. Make the most of torrent naming conventions to narrow down your searches.
#
# - Using a cache site (e.g, --cache=itorrents) is highly recommended since the generated magent links do not contain
#   any tracker info.
#
# [1]: magnet:?xt=urn:btih:ulfihylx35oldftn7qosmk6hkhsjq5af
# [2]: https://the-eye.eu/public/Random/rarbg/rarbg_db.zip
# [3]: https://www.sqlite.org/lang_expr.html#like

: "${RARBG_DB_PATH=$HOME/Downloads/rarbg_db.sqlite}"

usage()
{
	echo "Usage: $0 [command] PATTERN..." 1>&2
	echo 'Commands:' 1>&2
	echo -e '  --search\t\tList matching torrents in tabular format (default).' 1>&2
	echo -e "  --hash\t\tOutput just each torrent's info hash." 1>&2
	echo -e '  --magnet\t\tOutput a magnet link for each matching torrent.' 1>&2
	echo -e '  --cache=itorrents\tDownload .torrent files from iTorrents.org.' 1>&2
	echo -e '  --cache=btcache\tOpen browser tabs to download .torrent files from btcache.me.' 1>&2
	exit 2
}

escape_sql_string()
{
	printf -- '%s' "$1" | sed --regexp-extended "s/'/''/g"
}

url_encode()
{
	printf -- '%s' "$1" | perl -pe 's/([^0-9a-zA-Z])/sprintf("%%%02X", ord($1))/egs'
}

human_size()
{
	if [[ -n $1 ]]
	then
		numfmt --format='%fB' --to=iec-i "$1"
	fi
}

desktop_open()
{
	if hash xdg-open 2> /dev/null
	then
		xdg-open "$1"
	elif hash open 2> /dev/null
	then
		open "$1"
	else
		echo "Could not find an application to open $1" 1>&2
		exit 1
	fi
}

case $1 in
--*)
	operation=$1
	patterns=("${@:2}")
	;;
*)
	operation=--search
	patterns=("${@:1}")
	;;
esac

if [[ ${#patterns[@]} -eq 0 ]]
then
	usage
fi

for pattern in "${patterns[@]}"
do
	# we put title last so that vertical bars therein will be ignored
	safe_pattern=$(escape_sql_string "$pattern")
	query="SELECT hash, dt, size, title FROM items WHERE title LIKE '$safe_pattern' ESCAPE '\\' ORDER BY dt DESC;"
	sqlite3 -- "$RARBG_DB_PATH" "$query" | while IFS='|' read -r hash date size title
	do
		case $operation in
		--search)
			printf -- '%s\t%s\t%s\n' "$date" "$(human_size "$size")" "$title"
			;;
		--hash)
			printf -- '%s\n' "$hash"
			;;
		--magnet)
			printf -- 'magnet:?xt=urn:btih:%s&dn=%s\n' "$(url_encode "$hash")" "$(url_encode "$title")"
			;;
		--cache=itorrents)
			url=http://itorrents.org/torrent/$(url_encode "$hash").torrent
			wget --output-document="$title.torrent" -- "$url"
			;;
		--cache=btcache)
			url=https://btcache.me/torrent/$(url_encode "$hash")
			desktop_open "$url"
			;;
		*)
			echo "invalid operation: $operation" 1>&2
			usage
			;;
		esac
	done
done
